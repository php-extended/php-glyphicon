# php-extended/php-glyphicon
A library to insert more easily glyphicons to your php templates

![coverage](https://gitlab.com/php-extended/php-glyphicon/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-glyphicon/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-glyphicon ^8`

IMPORTANT NOTE : This library does not provide any font for the glyphicons.
You must find them yourself and add them to your html pages by yourself. 
[Bootstrap 3](http://getbootstrap.com/components/#glyphicons) provides such
icons you can add to your html pages. 


## Basic Usage

This library is only intended to reduce typing when creating templates. In 
such php templates, you can use :

```php

use PhpExtended\Glyphicon\Glyphicon as G;

echo G::EURO;	// echoes 'euro'
echo G::EURO();	// echoes '<span class="glyphicon glyphicon-euro" aria-hidden="true"></span>'

echo G::make(G::EURO);	// equivalent to G::EURO()

```


## License

MIT (See [license file](LICENSE)).
