<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-glyphicon library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Glyphicon;

use Stringable;

/**
 * Glyphicon class file.
 *
 * This class provides static elements that represents the glyphicon objects.
 *
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ExcessiveClassLength")
 * @SuppressWarnings("PHPMD.ExcessivePublicCount")
 * @SuppressWarnings("PHPMD.TooManyMethods")
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 * @SuppressWarnings("PHPMD.TooManyPublicMethods")
 * @SuppressWarnings("PHPMD.CamelCaseMethodName")
 * @SuppressWarnings("PHPMD.ShortMethodName")
 */
class Glyphicon implements Stringable
{
	
	// provided by bootstrap 3
	
	public const ASTERISK = 'asterisk';
	public const PLUS = 'plus';
	public const EURO = 'euro';
	public const MINUS = 'minus';
	public const CLOUD = 'cloud';
	public const ENVELOPE = 'envelope';
	public const PENCIL = 'pencil';
	public const GLASS = 'glass';
	public const MUSIC = 'music';
	public const SEARCH = 'search';
	public const HEART = 'heart';
	public const STAR = 'star';
	public const STAR_EMPTY = 'star-empty';
	public const USER = 'user';
	public const FILM = 'film';
	public const TH_LARGE = 'th-large';
	public const TH = 'th';
	public const TH_LIST = 'th-list';
	public const OK = 'ok';
	public const REMOVE = 'remove';
	public const ZOOM_IN = 'zoom-in';
	public const ZOOM_OUT = 'zoom-out';
	public const OFF = 'off';
	public const SIGNAL = 'signal';
	public const COG = 'cog';
	public const TRASH = 'trash';
	public const HOME = 'home';
	public const FILE = 'file';
	public const TIME = 'time';
	public const ROAD = 'road';
	public const DOWNLOAD_ALT = 'download-alt';
	public const DOWNLOAD = 'download';
	public const UPLOAD = 'upload';
	public const INBOX = 'inbox';
	public const PLAY_CIRCLE = 'play-circle';
	public const REPEAT = 'repeat';
	public const REFRESH = 'refresh';
	public const LIST_ALT = 'list-alt';
	public const LOCK = 'lock';
	public const FLAG = 'flag';
	public const HEADPHONES = 'headphones';
	public const VOLUME_OFF = 'volume-off';
	public const VOLUME_DOWN = 'volume-down';
	public const VOLUME_UP = 'volume-up';
	public const QRCODE = 'qrcode';
	public const BARCODE = 'barcode';
	public const TAG = 'tag';
	public const TAGS = 'tags';
	public const BOOK = 'book';
	public const BOOKMARK = 'bookmark';
	public const PRINTER = 'print';
	public const CAMERA = 'camera';
	public const FONT = 'font';
	public const BOLD = 'bold';
	public const ITALIC = 'italic';
	public const TEXT_HEIGHT = 'text-height';
	public const TEXT_WIDTH = 'text-width';
	public const ALIGN_LEFT = 'align-left';
	public const ALIGN_CENTER = 'align-center';
	public const ALIGN_RIGHT = 'align-right';
	public const ALIGN_JUSTIFY = 'align-justify';
	public const LISTED = 'list';
	public const INDENT_LEFT = 'indent-left';
	public const INDENT_RIGHT = 'indent-right';
	public const FACETIME = 'facetime-video';
	public const PICTURE = 'picture';
	public const MAP_MARKER = 'map-marker';
	public const ADJUST = 'adjust';
	public const TINT = 'tint';
	public const EDIT = 'edit';
	public const SHARE = 'share';
	public const CHECK = 'check';
	public const MOVE = 'move';
	public const STEP_BACKWARD = 'step-backward';
	public const FAST_BACKWARD = 'fast-backward';
	public const BACKWARD = 'backward';
	public const PLAY = 'play';
	public const PAUSE = 'pause';
	public const STOP = 'stop';
	public const FORWARD = 'forward';
	public const FAST_FORWARD = 'fast-forward';
	public const STEP_FORWARD = 'step-forward';
	public const EJECT = 'eject';
	public const CHEVRON_LEFT = 'chevron-left';
	public const CHEVRON_RIGHT = 'chevron-right';
	public const PLUS_SIGN = 'plus-sign';
	public const MINUS_SIGN = 'minus-sign';
	public const REMOVE_SIGN = 'remove-sign';
	public const OK_SIGN = 'ok-sign';
	public const QUESTION_SIGN = 'question-sign';
	public const INFO_SIGN = 'info-sign';
	public const SCREENSHOT = 'screenshot';
	public const REMOVE_CIRCLE = 'remove-circle';
	public const OK_CIRCLE = 'ok-circle';
	public const BAN_CIRCLE = 'ban-circle';
	public const ARROW_LEFT = 'arrow-left';
	public const ARROW_RIGHT = 'arrow-right';
	public const ARROW_UP = 'arrow-up';
	public const ARROW_DOWN = 'arrow-down';
	public const SHARE_ALT = 'share-alt';
	public const RESIZE_FULL = 'resize-full';
	public const RESIZE_SMALL = 'resize-small';
	public const EXCLAMATION_SIGN = 'exclamation-sign';
	public const GIFT = 'gift';
	public const LEAF = 'leaf';
	public const FIRE = 'fire';
	public const EYE_OPEN = 'eye-open';
	public const EYE_CLOSE = 'eye-close';
	public const WARNING_SIGN = 'warning-sign';
	public const PLANE = 'plane';
	public const CALENDAR = 'calendar';
	public const RANDOM = 'random';
	public const COMMENT = 'comment';
	public const MAGNET = 'magnet';
	public const CHEVRON_UP = 'chevron-up';
	public const CHEVRON_DOWN = 'chevron-down';
	public const RETWEET = 'retweet';
	public const SHOPPING_CART = 'shopping-cart';
	public const FOLDER_CLOSE = 'folder-close';
	public const FOLDER_OPEN = 'folder-open';
	public const RESIZE_VERTICAL = 'resize-vertical';
	public const RESIZE_HORIZONTAL = 'resize-horizontal';
	public const HDD = 'hdd';
	public const BULLHORN = 'bullhorn';
	public const BELL = 'bell';
	public const CERTIFICATE = 'certificate';
	public const THUMBS_UP = 'thumbs-up';
	public const THUMBS_DOWN = 'thumbs-down';
	public const HAND_RIGHT = 'hand-right';
	public const HAND_LEFT = 'hand-left';
	public const HAND_UP = 'hand-up';
	public const HAND_DOWN = 'hand-down';
	public const CIRCLE_ARROW_RIGHT = 'circle-arrow-right';
	public const CIRCLE_ARROW_LEFT = 'circle-arrow-left';
	public const CIRCLE_ARROW_UP = 'circle-arrow-up';
	public const CIRCLE_ARROW_DOWN = 'circle-arrow-down';
	public const GLOBE = 'globe';
	public const WRENCH = 'wrench';
	public const TASKS = 'tasks';
	public const FILTER = 'filter';
	public const BRIEFCASE = 'briefcase';
	public const FULLSCREEN = 'fullscreen';
	public const DASHBOARD = 'dashboard';
	public const PAPERCLIP = 'paperclip';
	public const HEART_EMPTY = 'heart-empty';
	public const LINK = 'link';
	public const PHONE = 'phone';
	public const PUSHPIN = 'pushpin';
	public const USD = 'usd';
	public const GPB = 'gbp';
	public const SORT = 'sort';
	public const SORT_ALPHABET = 'sort-by-alphabet';
	public const SORT_ALPHABET_ALT = 'sort-by-alphabet-alt';
	public const SORT_ORDER = 'sort-by-order';
	public const SORT_ORDER_ALT = 'sort-by-order-alt';
	public const SORT_ATTRIBUTES = 'sort-by-attributes';
	public const SORT_ATTRIBUTES_ALT = 'sort-by-attributes-alt';
	public const UNCHECKED = 'unchecked';
	public const EXPAND = 'expand';
	public const COLLAPSE_DOWN = 'collapse-down';
	public const COLLAPSE_UP = 'collapse-up';
	public const LOG_IN = 'log-in';
	public const FLASH = 'flash';
	public const LOG_OUT = 'log-out';
	public const NEW_WINDOW = 'new-window';
	public const RECORD = 'record';
	public const SAVE = 'save';
	public const OPEN = 'open';
	public const SAVED = 'saved';
	public const IMPORT = 'import';
	public const EXPORT = 'export';
	public const SEND = 'send';
	public const FLOPPY_DISK = 'floppy-disk';
	public const FLOPPY_SAVED = 'floppy-saved';
	public const FLOPPY_REMOVE = 'floppy-remove';
	public const FLOPPY_SAVE = 'floppy-save';
	public const FLOPPY_OPEN = 'floppy-open';
	public const CREDIT_CARD = 'credit-card';
	public const TRANSFER = 'transfer';
	public const CUTLERY = 'cutlery';
	public const HEADER = 'header';
	public const COMPRESSED = 'compressed';
	public const EARPHONE = 'earphone';
	public const PHONE_ALT = 'phone-alt';
	public const TOWER = 'tower';
	public const STATS = 'stats';
	public const VIDEO_SD = 'sd-video';
	public const VIDEO_HD = 'hd-video';
	public const SUBTITLES = 'subtitles';
	public const SOUND_STEREO = 'sound-stereo';
	public const SOUND_DOLBY = 'sound-dolby';
	public const SOUND_5_1 = 'sound-5-1';
	public const SOUND_6_1 = 'sound-6-1';
	public const SOUND_7_1 = 'sound-7-1';
	public const COPYRIGHT_MARK = 'copyright-mark';
	public const REGISTRATION_MARK = 'registration-mark';
	public const CLOUD_DOWNLOAD = 'cloud-download';
	public const CLOUD_UPLOAD = 'cloud-upload';
	public const TREE_CONIFER = 'tree-conifer';
	public const TREE_DECIDUOUS = 'tree-deciduous';
	public const CD = 'cd';
	public const FILE_SAVE = 'save-file';
	public const FILE_OPEN = 'open-file';
	public const LEVEL_UP = 'level-up';
	public const COPY = 'copy';
	public const PASTE = 'paste';
	public const ALERT = 'alert';
	public const EQUALIZER = 'equalizer';
	public const KING = 'king';
	public const QUEEN = 'queen';
	public const PAWN = 'pawn';
	public const BISHOP = 'bishop';
	public const KNIGHT = 'knight';
	public const BABY_FORMULA = 'baby-formula';
	public const TENT = 'tent';
	public const BLACKBOARD = 'blackboard';
	public const BED = 'bed';
	public const APPLE = 'apple';
	public const ERASE = 'erase';
	public const HOURGLASS = 'hourglass';
	public const LAMP = 'lamp';
	public const DUPLICATE = 'duplicate';
	public const PIGGY_BANK = 'piggy-bank';
	public const SCISSORS = 'scissors';
	public const BITCOIN = 'bitcoin';
	public const YEN = 'yen';
	public const RUBLE = 'ruble';
	public const SCALE = 'scale';
	public const ICE_LOLLY = 'ice-lolly';
	public const ICE_LOLLY_TASTED = 'ice-lolly-tasted';
	public const EDUCATION = 'education';
	public const OPTION_HORIZONTAL = 'option-horizontal';
	public const OPTION_VERTICAL = 'option-vertical';
	public const MENU_HAMBURGER = 'menu-hamburger';
	public const MODAL_WINDOW = 'modal-window';
	public const OIL = 'oil';
	public const GRAIN = 'grain';
	public const SUNGLASSES = 'sunglasses';
	public const TEXT_SIZE = 'text-size';
	public const TEXT_COLOR = 'text-color';
	public const TEXT_BACKGROUND = 'text-background';
	public const OBJ_ALIGN_TOP = 'object-align-top';
	public const OBJ_ALIGN_BOTTOM = 'object-align-bottom';
	public const OBJ_ALIGN_HORIZONTAL = 'object-align-horizontal';
	public const OBJ_ALIGN_LEFT = 'object-align-left';
	public const OBJ_ALIGN_VERTICAL = 'object-align-vertical';
	public const OBJ_ALIGN_RIGHT = 'object-align-right';
	public const TRIANGLE_RIGHT = 'triangle-right';
	public const TRIANGLE_LEFT = 'triangle-left';
	public const TRIANGLE_BOTTOM = 'triangle-bottom';
	public const TRIANGLE_TOP = 'triangle-top';
	public const CONSOLE = 'console';
	public const SUPERSCRIPT = 'superscript';
	public const SUBSCRIPT = 'subscript';
	public const MENU_LEFT = 'menu-left';
	public const MENU_RIGHT = 'menu-right';
	public const MENU_DOWN = 'menu-down';
	public const MENU_UP = 'menu-up ';
	
	/**
	 * Builds a new Glyphicon html string with the given symbol constant name.
	 *
	 * @param string $symbol
	 * @return string
	 */
	public static function make(string $symbol) : string
	{
		return '<span class="glyphicon glyphicon-'.$symbol.' aria-hidden="true"></span>';
	}
	
	public static function ASTERISK() : string
	{
		return self::make(Glyphicon::ASTERISK);
	}
	
	public static function PLUS() : string
	{
		return self::make(Glyphicon::PLUS);
	}
	
	public static function EURO() : string
	{
		return self::make(Glyphicon::EURO);
	}
	
	public static function MINUS() : string
	{
		return self::make(Glyphicon::MINUS);
	}
	
	public static function CLOUD() : string
	{
		return self::make(Glyphicon::CLOUD);
	}
	
	public static function ENVELOPE() : string
	{
		return self::make(Glyphicon::ENVELOPE);
	}
	
	public static function PENCIL() : string
	{
		return self::make(Glyphicon::PENCIL);
	}
	
	public static function GLASS() : string
	{
		return self::make(Glyphicon::GLASS);
	}
	
	public static function MUSIC() : string
	{
		return self::make(Glyphicon::MUSIC);
	}
	
	public static function SEARCH() : string
	{
		return self::make(Glyphicon::SEARCH);
	}
	
	public static function HEART() : string
	{
		return self::make(Glyphicon::HEART);
	}
	
	public static function STAR() : string
	{
		return self::make(Glyphicon::STAR);
	}
	
	public static function STAR_EMPTY() : string
	{
		return self::make(Glyphicon::STAR_EMPTY);
	}
	
	public static function USER() : string
	{
		return self::make(Glyphicon::USER);
	}
	
	public static function FILM() : string
	{
		return self::make(Glyphicon::FILM);
	}
	
	public static function TH_LARGE() : string
	{
		return self::make(Glyphicon::TH_LARGE);
	}
	
	public static function TH() : string
	{
		return self::make(Glyphicon::TH);
	}
	
	public static function TH_LIST() : string
	{
		return self::make(Glyphicon::TH_LIST);
	}
	
	public static function OK() : string
	{
		return self::make(Glyphicon::OK);
	}
	
	public static function REMOVE() : string
	{
		return self::make(Glyphicon::REMOVE);
	}
	
	public static function ZOOM_IN() : string
	{
		return self::make(Glyphicon::ZOOM_IN);
	}
	
	public static function ZOOM_OUT() : string
	{
		return self::make(Glyphicon::ZOOM_OUT);
	}
	
	public static function OFF() : string
	{
		return self::make(Glyphicon::OFF);
	}
	
	public static function SIGNAL() : string
	{
		return self::make(Glyphicon::SIGNAL);
	}
	
	public static function COG() : string
	{
		return self::make(Glyphicon::COG);
	}
	
	public static function TRASH() : string
	{
		return self::make(Glyphicon::TRASH);
	}
	
	public static function HOME() : string
	{
		return self::make(Glyphicon::HOME);
	}
	
	public static function FILE() : string
	{
		return self::make(Glyphicon::FILE);
	}
	
	public static function TIME() : string
	{
		return self::make(Glyphicon::TIME);
	}
	
	public static function ROAD() : string
	{
		return self::make(Glyphicon::ROAD);
	}
	
	public static function DOWNLOAD_ALT() : string
	{
		return self::make(Glyphicon::DOWNLOAD_ALT);
	}
	
	public static function DOWNLOAD() : string
	{
		return self::make(Glyphicon::DOWNLOAD);
	}
	
	public static function UPLOAD() : string
	{
		return self::make(Glyphicon::UPLOAD);
	}
	
	public static function INBOX() : string
	{
		return self::make(Glyphicon::INBOX);
	}
	
	public static function PLAY_CIRCLE() : string
	{
		return self::make(Glyphicon::PLAY_CIRCLE);
	}
	
	public static function REPEAT() : string
	{
		return self::make(Glyphicon::REPEAT);
	}
	
	public static function REFRESH() : string
	{
		return self::make(Glyphicon::REFRESH);
	}
	
	public static function LIST_ALT() : string
	{
		return self::make(Glyphicon::LIST_ALT);
	}
	
	public static function LOCK() : string
	{
		return self::make(Glyphicon::LOCK);
	}
	
	public static function FLAG() : string
	{
		return self::make(Glyphicon::FLAG);
	}
	
	public static function HEADPHONES() : string
	{
		return self::make(Glyphicon::HEADPHONES);
	}
	
	public static function VOLUME_OFF() : string
	{
		return self::make(Glyphicon::VOLUME_OFF);
	}
	
	public static function VOLUME_DOWN() : string
	{
		return self::make(Glyphicon::VOLUME_DOWN);
	}
	
	public static function VOLUME_UP() : string
	{
		return self::make(Glyphicon::VOLUME_UP);
	}
	
	public static function QRCODE() : string
	{
		return self::make(Glyphicon::QRCODE);
	}
	
	public static function BARCODE() : string
	{
		return self::make(Glyphicon::BARCODE);
	}
	
	public static function TAG() : string
	{
		return self::make(Glyphicon::TAG);
	}
	
	public static function TAGS() : string
	{
		return self::make(Glyphicon::TAGS);
	}
	
	public static function BOOK() : string
	{
		return self::make(Glyphicon::BOOK);
	}
	
	public static function BOOKMARK() : string
	{
		return self::make(Glyphicon::BOOKMARK);
	}
	
	public static function PRINTER() : string
	{
		return self::make(Glyphicon::PRINTER);
	}
	
	public static function CAMERA() : string
	{
		return self::make(Glyphicon::CAMERA);
	}
	
	public static function FONT() : string
	{
		return self::make(Glyphicon::FONT);
	}
	
	public static function BOLD() : string
	{
		return self::make(Glyphicon::BOLD);
	}
	
	public static function ITALIC() : string
	{
		return self::make(Glyphicon::ITALIC);
	}
	
	public static function TEXT_HEIGHT() : string
	{
		return self::make(Glyphicon::TEXT_HEIGHT);
	}
	
	public static function TEXT_WIDTH() : string
	{
		return self::make(Glyphicon::TEXT_WIDTH);
	}
	
	public static function ALIGN_LEFT() : string
	{
		return self::make(Glyphicon::ALIGN_LEFT);
	}
	
	public static function ALIGN_CENTER() : string
	{
		return self::make(Glyphicon::ALIGN_CENTER);
	}
	
	public static function ALIGN_RIGHT() : string
	{
		return self::make(Glyphicon::ALIGN_RIGHT);
	}
	
	public static function ALIGN_JUSTIFY() : string
	{
		return self::make(Glyphicon::ALIGN_JUSTIFY);
	}
	
	public static function LISTED() : string
	{
		return self::make(Glyphicon::LISTED);
	}
	
	public static function INDENT_LEFT() : string
	{
		return self::make(Glyphicon::INDENT_LEFT);
	}
	
	public static function INDENT_RIGHT() : string
	{
		return self::make(Glyphicon::INDENT_RIGHT);
	}
	
	public static function FACETIME() : string
	{
		return self::make(Glyphicon::FACETIME);
	}
	
	public static function PICTURE() : string
	{
		return self::make(Glyphicon::PICTURE);
	}
	
	public static function MAP_MARKER() : string
	{
		return self::make(Glyphicon::MAP_MARKER);
	}
	
	public static function ADJUST() : string
	{
		return self::make(Glyphicon::ADJUST);
	}
	
	public static function TINT() : string
	{
		return self::make(Glyphicon::TINT);
	}
	
	public static function EDIT() : string
	{
		return self::make(Glyphicon::EDIT);
	}
	
	public static function SHARE() : string
	{
		return self::make(Glyphicon::SHARE);
	}
	
	public static function CHECK() : string
	{
		return self::make(Glyphicon::CHECK);
	}
	
	public static function MOVE() : string
	{
		return self::make(Glyphicon::MOVE);
	}
	
	public static function STEP_BACKWARD() : string
	{
		return self::make(Glyphicon::STEP_BACKWARD);
	}
	
	public static function FAST_BACKWARD() : string
	{
		return self::make(Glyphicon::FAST_BACKWARD);
	}
	
	public static function BACKWARD() : string
	{
		return self::make(Glyphicon::BACKWARD);
	}
	
	public static function PLAY() : string
	{
		return self::make(Glyphicon::PLAY);
	}
	
	public static function PAUSE() : string
	{
		return self::make(Glyphicon::PAUSE);
	}
	
	public static function STOP() : string
	{
		return self::make(Glyphicon::STOP);
	}
	
	public static function FORWARD() : string
	{
		return self::make(Glyphicon::FORWARD);
	}
	
	public static function FAST_FORWARD() : string
	{
		return self::make(Glyphicon::FAST_FORWARD);
	}
	
	public static function STEP_FORWARD() : string
	{
		return self::make(Glyphicon::STEP_FORWARD);
	}
	
	public static function EJECT() : string
	{
		return self::make(Glyphicon::EJECT);
	}
	
	public static function CHEVRON_LEFT() : string
	{
		return self::make(Glyphicon::CHEVRON_LEFT);
	}
	
	public static function CHEVRON_RIGHT() : string
	{
		return self::make(Glyphicon::CHEVRON_RIGHT);
	}
	
	public static function PLUS_SIGN() : string
	{
		return self::make(Glyphicon::PLUS_SIGN);
	}
	
	public static function MINUS_SIGN() : string
	{
		return self::make(Glyphicon::MINUS_SIGN);
	}
	
	public static function REMOVE_SIGN() : string
	{
		return self::make(Glyphicon::REMOVE_SIGN);
	}
	
	public static function OK_SIGN() : string
	{
		return self::make(Glyphicon::OK_SIGN);
	}
	
	public static function QUESTION_SIGN() : string
	{
		return self::make(Glyphicon::QUESTION_SIGN);
	}
	
	public static function INFO_SIGN() : string
	{
		return self::make(Glyphicon::INFO_SIGN);
	}
	
	public static function SCREENSHOT() : string
	{
		return self::make(Glyphicon::SCREENSHOT);
	}
	
	public static function REMOVE_CIRCLE() : string
	{
		return self::make(Glyphicon::REMOVE_CIRCLE);
	}
	
	public static function OK_CIRCLE() : string
	{
		return self::make(Glyphicon::OK_CIRCLE);
	}
	
	public static function BAN_CIRCLE() : string
	{
		return self::make(Glyphicon::BAN_CIRCLE);
	}
	
	public static function ARROW_LEFT() : string
	{
		return self::make(Glyphicon::ARROW_LEFT);
	}
	
	public static function ARROW_RIGHT() : string
	{
		return self::make(Glyphicon::ARROW_RIGHT);
	}
	
	public static function ARROW_UP() : string
	{
		return self::make(Glyphicon::ARROW_UP);
	}
	
	public static function ARROW_DOWN() : string
	{
		return self::make(Glyphicon::ARROW_DOWN);
	}
	
	public static function SHARE_ALT() : string
	{
		return self::make(Glyphicon::SHARE_ALT);
	}
	
	public static function RESIZE_FULL() : string
	{
		return self::make(Glyphicon::RESIZE_FULL);
	}
	
	public static function RESIZE_SMALL() : string
	{
		return self::make(Glyphicon::RESIZE_SMALL);
	}
	
	public static function EXCLAMATION_SIGN() : string
	{
		return self::make(Glyphicon::EXCLAMATION_SIGN);
	}
	
	public static function GIFT() : string
	{
		return self::make(Glyphicon::GIFT);
	}
	
	public static function LEAF() : string
	{
		return self::make(Glyphicon::LEAF);
	}
	
	public static function FIRE() : string
	{
		return self::make(Glyphicon::FIRE);
	}
	
	public static function EYE_OPEN() : string
	{
		return self::make(Glyphicon::EYE_OPEN);
	}
	
	public static function EYE_CLOSE() : string
	{
		return self::make(Glyphicon::EYE_CLOSE);
	}
	
	public static function WARNING_SIGN() : string
	{
		return self::make(Glyphicon::WARNING_SIGN);
	}
	
	public static function PLANE() : string
	{
		return self::make(Glyphicon::PLANE);
	}
	
	public static function CALENDAR() : string
	{
		return self::make(Glyphicon::CALENDAR);
	}
	
	public static function RANDOM() : string
	{
		return self::make(Glyphicon::RANDOM);
	}
	
	public static function COMMENT() : string
	{
		return self::make(Glyphicon::COMMENT);
	}
	
	public static function MAGNET() : string
	{
		return self::make(Glyphicon::MAGNET);
	}
	
	public static function CHEVRON_UP() : string
	{
		return self::make(Glyphicon::CHEVRON_UP);
	}
	
	public static function CHEVRON_DOWN() : string
	{
		return self::make(Glyphicon::CHEVRON_DOWN);
	}
	
	public static function RETWEET() : string
	{
		return self::make(Glyphicon::RETWEET);
	}
	
	public static function SHOPPING_CART() : string
	{
		return self::make(Glyphicon::SHOPPING_CART);
	}
	
	public static function FOLDER_CLOSE() : string
	{
		return self::make(Glyphicon::FOLDER_CLOSE);
	}
	
	public static function FOLDER_OPEN() : string
	{
		return self::make(Glyphicon::FOLDER_OPEN);
	}
	
	public static function RESIZE_VERTICAL() : string
	{
		return self::make(Glyphicon::RESIZE_VERTICAL);
	}
	
	public static function RESIZE_HORIZONTAL() : string
	{
		return self::make(Glyphicon::RESIZE_HORIZONTAL);
	}
	
	public static function HDD() : string
	{
		return self::make(Glyphicon::HDD);
	}
	
	public static function BULLHORN() : string
	{
		return self::make(Glyphicon::BULLHORN);
	}
	
	public static function BELL() : string
	{
		return self::make(Glyphicon::BELL);
	}
	
	public static function CERTIFICATE() : string
	{
		return self::make(Glyphicon::CERTIFICATE);
	}
	
	public static function THUMBS_UP() : string
	{
		return self::make(Glyphicon::THUMBS_UP);
	}
	
	public static function THUMBS_DOWN() : string
	{
		return self::make(Glyphicon::THUMBS_DOWN);
	}
	
	public static function HAND_RIGHT() : string
	{
		return self::make(Glyphicon::HAND_RIGHT);
	}
	
	public static function HAND_LEFT() : string
	{
		return self::make(Glyphicon::HAND_LEFT);
	}
	
	public static function HAND_UP() : string
	{
		return self::make(Glyphicon::HAND_UP);
	}
	
	public static function HAND_DOWN() : string
	{
		return self::make(Glyphicon::HAND_DOWN);
	}
	
	public static function CIRCLE_ARROW_RIGHT() : string
	{
		return self::make(Glyphicon::CIRCLE_ARROW_RIGHT);
	}
	
	public static function CIRCLE_ARROW_LEFT() : string
	{
		return self::make(Glyphicon::CIRCLE_ARROW_LEFT);
	}
	
	public static function CIRCLE_ARROW_UP() : string
	{
		return self::make(Glyphicon::CIRCLE_ARROW_UP);
	}
	
	public static function CIRCLE_ARROW_DOWN() : string
	{
		return self::make(Glyphicon::CIRCLE_ARROW_DOWN);
	}
	
	public static function GLOBE() : string
	{
		return self::make(Glyphicon::GLOBE);
	}
	
	public static function WRENCH() : string
	{
		return self::make(Glyphicon::WRENCH);
	}
	
	public static function TASKS() : string
	{
		return self::make(Glyphicon::TASKS);
	}
	
	public static function FILTER() : string
	{
		return self::make(Glyphicon::FILTER);
	}
	
	public static function BRIEFCASE() : string
	{
		return self::make(Glyphicon::BRIEFCASE);
	}
	
	public static function FULLSCREEN() : string
	{
		return self::make(Glyphicon::FULLSCREEN);
	}
	
	public static function DASHBOARD() : string
	{
		return self::make(Glyphicon::DASHBOARD);
	}
	
	public static function PAPERCLIP() : string
	{
		return self::make(Glyphicon::PAPERCLIP);
	}
	
	public static function HEART_EMPTY() : string
	{
		return self::make(Glyphicon::HEART_EMPTY);
	}
	
	public static function LINK() : string
	{
		return self::make(Glyphicon::LINK);
	}
	
	public static function PHONE() : string
	{
		return self::make(Glyphicon::PHONE);
	}
	
	public static function PUSHPIN() : string
	{
		return self::make(Glyphicon::PUSHPIN);
	}
	
	public static function USD() : string
	{
		return self::make(Glyphicon::USD);
	}
	
	public static function GPB() : string
	{
		return self::make(Glyphicon::GPB);
	}
	
	public static function SORT() : string
	{
		return self::make(Glyphicon::SORT);
	}
	
	public static function SORT_ALPHABET() : string
	{
		return self::make(Glyphicon::SORT_ALPHABET);
	}
	
	public static function SORT_ALPHABET_ALT() : string
	{
		return self::make(Glyphicon::SORT_ALPHABET_ALT);
	}
	
	public static function SORT_ORDER() : string
	{
		return self::make(Glyphicon::SORT_ORDER);
	}
	
	public static function SORT_ORDER_ALT() : string
	{
		return self::make(Glyphicon::SORT_ORDER_ALT);
	}
	
	public static function SORT_ATTRIBUTES() : string
	{
		return self::make(Glyphicon::SORT_ATTRIBUTES);
	}
	
	public static function SORT_ATTRIBUTES_ALT() : string
	{
		return self::make(Glyphicon::SORT_ATTRIBUTES_ALT);
	}
	
	public static function UNCHECKED() : string
	{
		return self::make(Glyphicon::UNCHECKED);
	}
	
	public static function EXPAND() : string
	{
		return self::make(Glyphicon::EXPAND);
	}
	
	public static function COLLAPSE_DOWN() : string
	{
		return self::make(Glyphicon::COLLAPSE_DOWN);
	}
	
	public static function COLLAPSE_UP() : string
	{
		return self::make(Glyphicon::COLLAPSE_UP);
	}
	
	public static function LOG_IN() : string
	{
		return self::make(Glyphicon::LOG_IN);
	}
	
	public static function FLASH() : string
	{
		return self::make(Glyphicon::FLASH);
	}
	
	public static function LOG_OUT() : string
	{
		return self::make(Glyphicon::LOG_OUT);
	}
	
	public static function NEW_WINDOW() : string
	{
		return self::make(Glyphicon::NEW_WINDOW);
	}
	
	public static function RECORD() : string
	{
		return self::make(Glyphicon::RECORD);
	}
	
	public static function SAVE() : string
	{
		return self::make(Glyphicon::SAVE);
	}
	
	public static function OPEN() : string
	{
		return self::make(Glyphicon::OPEN);
	}
	
	public static function SAVED() : string
	{
		return self::make(Glyphicon::SAVED);
	}
	
	public static function IMPORT() : string
	{
		return self::make(Glyphicon::IMPORT);
	}
	
	public static function EXPORT() : string
	{
		return self::make(Glyphicon::EXPORT);
	}
	
	public static function SEND() : string
	{
		return self::make(Glyphicon::SEND);
	}
	
	public static function FLOPPY_DISK() : string
	{
		return self::make(Glyphicon::FLOPPY_DISK);
	}
	
	public static function FLOPPY_SAVED() : string
	{
		return self::make(Glyphicon::FLOPPY_SAVED);
	}
	
	public static function FLOPPY_REMOVE() : string
	{
		return self::make(Glyphicon::FLOPPY_REMOVE);
	}
	
	public static function FLOPPY_SAVE() : string
	{
		return self::make(Glyphicon::FLOPPY_SAVE);
	}
	
	public static function FLOPPY_OPEN() : string
	{
		return self::make(Glyphicon::FLOPPY_OPEN);
	}
	
	public static function CREDIT_CARD() : string
	{
		return self::make(Glyphicon::CREDIT_CARD);
	}
	
	public static function TRANSFER() : string
	{
		return self::make(Glyphicon::TRANSFER);
	}
	
	public static function CUTLERY() : string
	{
		return self::make(Glyphicon::CUTLERY);
	}
	
	public static function HEADER() : string
	{
		return self::make(Glyphicon::HEADER);
	}
	
	public static function COMPRESSED() : string
	{
		return self::make(Glyphicon::COMPRESSED);
	}
	
	public static function EARPHONE() : string
	{
		return self::make(Glyphicon::EARPHONE);
	}
	
	public static function PHONE_ALT() : string
	{
		return self::make(Glyphicon::PHONE_ALT);
	}
	
	public static function TOWER() : string
	{
		return self::make(Glyphicon::TOWER);
	}
	
	public static function STATS() : string
	{
		return self::make(Glyphicon::STATS);
	}
	
	public static function VIDEO_SD() : string
	{
		return self::make(Glyphicon::VIDEO_SD);
	}
	
	public static function VIDEO_HD() : string
	{
		return self::make(Glyphicon::VIDEO_HD);
	}
	
	public static function SUBTITLES() : string
	{
		return self::make(Glyphicon::SUBTITLES);
	}
	
	public static function SOUND_STEREO() : string
	{
		return self::make(Glyphicon::SOUND_STEREO);
	}
	
	public static function SOUND_DOLBY() : string
	{
		return self::make(Glyphicon::SOUND_DOLBY);
	}
	
	public static function SOUND_5_1() : string
	{
		return self::make(Glyphicon::SOUND_5_1);
	}
	
	public static function SOUND_6_1() : string
	{
		return self::make(Glyphicon::SOUND_6_1);
	}
	
	public static function SOUND_7_1() : string
	{
		return self::make(Glyphicon::SOUND_7_1);
	}
	
	public static function COPYRIGHT_MARK() : string
	{
		return self::make(Glyphicon::COPYRIGHT_MARK);
	}
	
	public static function REGISTRATION_MARK() : string
	{
		return self::make(Glyphicon::REGISTRATION_MARK);
	}
	
	public static function CLOUD_DOWNLOAD() : string
	{
		return self::make(Glyphicon::CLOUD_DOWNLOAD);
	}
	
	public static function CLOUD_UPLOAD() : string
	{
		return self::make(Glyphicon::CLOUD_UPLOAD);
	}
	
	public static function TREE_CONIFER() : string
	{
		return self::make(Glyphicon::TREE_CONIFER);
	}
	
	public static function TREE_DECIDUOUS() : string
	{
		return self::make(Glyphicon::TREE_DECIDUOUS);
	}
	
	public static function CD() : string
	{
		return self::make(Glyphicon::CD);
	}
	
	public static function FILE_SAVE() : string
	{
		return self::make(Glyphicon::FILE_SAVE);
	}
	
	public static function FILE_OPEN() : string
	{
		return self::make(Glyphicon::FILE_OPEN);
	}
	
	public static function LEVEL_UP() : string
	{
		return self::make(Glyphicon::LEVEL_UP);
	}
	
	public static function COPY() : string
	{
		return self::make(Glyphicon::COPY);
	}
	
	public static function PASTE() : string
	{
		return self::make(Glyphicon::PASTE);
	}
	
	public static function ALERT() : string
	{
		return self::make(Glyphicon::ALERT);
	}
	
	public static function EQUALIZER() : string
	{
		return self::make(Glyphicon::EQUALIZER);
	}
	
	public static function KING() : string
	{
		return self::make(Glyphicon::KING);
	}
	
	public static function QUEEN() : string
	{
		return self::make(Glyphicon::QUEEN);
	}
	
	public static function PAWN() : string
	{
		return self::make(Glyphicon::PAWN);
	}
	
	public static function BISHOP() : string
	{
		return self::make(Glyphicon::BISHOP);
	}
	
	public static function KNIGHT() : string
	{
		return self::make(Glyphicon::KNIGHT);
	}
	
	public static function BABY_FORMULA() : string
	{
		return self::make(Glyphicon::BABY_FORMULA);
	}
	
	public static function TENT() : string
	{
		return self::make(Glyphicon::TENT);
	}
	
	public static function BLACKBOARD() : string
	{
		return self::make(Glyphicon::BLACKBOARD);
	}
	
	public static function BED() : string
	{
		return self::make(Glyphicon::BED);
	}
	
	public static function APPLE() : string
	{
		return self::make(Glyphicon::APPLE);
	}
	
	public static function ERASE() : string
	{
		return self::make(Glyphicon::ERASE);
	}
	
	public static function HOURGLASS() : string
	{
		return self::make(Glyphicon::HOURGLASS);
	}
	
	public static function LAMP() : string
	{
		return self::make(Glyphicon::LAMP);
	}
	
	public static function DUPLICATE() : string
	{
		return self::make(Glyphicon::DUPLICATE);
	}
	
	public static function PIGGY_BANK() : string
	{
		return self::make(Glyphicon::PIGGY_BANK);
	}
	
	public static function SCISSORS() : string
	{
		return self::make(Glyphicon::SCISSORS);
	}
	
	public static function BITCOIN() : string
	{
		return self::make(Glyphicon::BITCOIN);
	}
	
	public static function YEN() : string
	{
		return self::make(Glyphicon::YEN);
	}
	
	public static function RUBLE() : string
	{
		return self::make(Glyphicon::RUBLE);
	}
	
	public static function SCALE() : string
	{
		return self::make(Glyphicon::SCALE);
	}
	
	public static function ICE_LOLLY() : string
	{
		return self::make(Glyphicon::ICE_LOLLY);
	}
	
	public static function ICE_LOLLY_TASTED() : string
	{
		return self::make(Glyphicon::ICE_LOLLY_TASTED);
	}
	
	public static function EDUCATION() : string
	{
		return self::make(Glyphicon::EDUCATION);
	}
	
	public static function OPTION_HORIZONTAL() : string
	{
		return self::make(Glyphicon::OPTION_HORIZONTAL);
	}
	
	public static function OPTION_VERTICAL() : string
	{
		return self::make(Glyphicon::OPTION_VERTICAL);
	}
	
	public static function MENU_HAMBURGER() : string
	{
		return self::make(Glyphicon::MENU_HAMBURGER);
	}
	
	public static function MODAL_WINDOW() : string
	{
		return self::make(Glyphicon::MODAL_WINDOW);
	}
	
	public static function OIL() : string
	{
		return self::make(Glyphicon::OIL);
	}
	
	public static function GRAIN() : string
	{
		return self::make(Glyphicon::GRAIN);
	}
	
	public static function SUNGLASSES() : string
	{
		return self::make(Glyphicon::SUNGLASSES);
	}
	
	public static function TEXT_SIZE() : string
	{
		return self::make(Glyphicon::TEXT_SIZE);
	}
	
	public static function TEXT_COLOR() : string
	{
		return self::make(Glyphicon::TEXT_COLOR);
	}
	
	public static function TEXT_BACKGROUND() : string
	{
		return self::make(Glyphicon::TEXT_BACKGROUND);
	}
	
	public static function OBJ_ALIGN_TOP() : string
	{
		return self::make(Glyphicon::OBJ_ALIGN_TOP);
	}
	
	public static function OBJ_ALIGN_BOTTOM() : string
	{
		return self::make(Glyphicon::OBJ_ALIGN_BOTTOM);
	}
	
	public static function OBJ_ALIGN_HORIZONTAL() : string
	{
		return self::make(Glyphicon::OBJ_ALIGN_HORIZONTAL);
	}
	
	public static function OBJ_ALIGN_LEFT() : string
	{
		return self::make(Glyphicon::OBJ_ALIGN_LEFT);
	}
	
	public static function OBJ_ALIGN_VERTICAL() : string
	{
		return self::make(Glyphicon::OBJ_ALIGN_VERTICAL);
	}
	
	public static function OBJ_ALIGN_RIGHT() : string
	{
		return self::make(Glyphicon::OBJ_ALIGN_RIGHT);
	}
	
	public static function TRIANGLE_RIGHT() : string
	{
		return self::make(Glyphicon::TRIANGLE_RIGHT);
	}
	
	public static function TRIANGLE_LEFT() : string
	{
		return self::make(Glyphicon::TRIANGLE_LEFT);
	}
	
	public static function TRIANGLE_BOTTOM() : string
	{
		return self::make(Glyphicon::TRIANGLE_BOTTOM);
	}
	
	public static function TRIANGLE_TOP() : string
	{
		return self::make(Glyphicon::TRIANGLE_TOP);
	}
	
	public static function CONSOLE() : string
	{
		return self::make(Glyphicon::CONSOLE);
	}
	
	public static function SUPERSCRIPT() : string
	{
		return self::make(Glyphicon::SUPERSCRIPT);
	}
	
	public static function SUBSCRIPT() : string
	{
		return self::make(Glyphicon::SUBSCRIPT);
	}
	
	public static function MENU_LEFT() : string
	{
		return self::make(Glyphicon::MENU_LEFT);
	}
	
	public static function MENU_RIGHT() : string
	{
		return self::make(Glyphicon::MENU_RIGHT);
	}
	
	public static function MENU_DOWN() : string
	{
		return self::make(Glyphicon::MENU_DOWN);
	}
	
	public static function MENU_UP() : string
	{
		return self::make(Glyphicon::MENU_UP);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
}
